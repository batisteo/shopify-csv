# Shopify CSV

> Recruiting task for Baptiste Darthenay as Senior Python Developer

## Getting Started

```bash
git clone git@gitlab.com:batisteo/shopify-csv.git && cd shopify-csv
```

### With Poetry
```bash
python3.10 -m venv .venv
poetry install
```

```bash
time poetry run scrap examples/stores.csv
# or
source .venv/bin/activate
scrap [path/to/file.csv]
```

### With Docker
```bash
docker-compose up --build
```

## How would you improve your solution?

This async code is hitting the servers quite hard, and it’s giving us
some timeouts. We should throttle to find a sweet point between
waiting and getting too many timeouts.

Error handling could be better, as well as a more decoupled layers,
especially the data and the network.  
This would help to write some tests, which I didn’t do this time.

## How did you measure performance?

Just running `time` in the terminal to check the overall taken time to run.

## Anything you want to mention?

I regret I didn’t took more time on this task, especially to build a more robust,
clean and tested solution.
