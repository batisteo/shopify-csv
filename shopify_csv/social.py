import asyncio
import re
from urllib.parse import urljoin

import httpx
from bs4 import BeautifulSoup

URL_PATHS = [
    "/",
    "/pages/about",
    "/pages/about-us",
    "/pages/contact",
    "/pages/contact-us",
]

BASE = r"https?:\/\/(www\.)?{domain}\/\w+\/?"
RE_TWITTER = re.compile(BASE.format(domain=r"twitter\.com"))
RE_FACEBOOK = re.compile(BASE.format(domain=r"(fb\.me|facebook\.com)"))
RE_EMAIL = re.compile(r"mailto:(?P<email>\S+@\S+\.\w+)")


async def find_social_links(client, base_url) -> dict:
    results = await asyncio.gather(
        *(load_page(client, urljoin(base_url, path)) for path in URL_PATHS)
    )
    links = {}
    for result in results:
        links = {**links, **result}
    return links


async def load_page(client, url) -> dict:
    links = {}
    try:
        response = await client.get(url)
    except httpx.TimeoutException:
        print(url, " Timeout load_page", flush=True)
        return {}
    try:
        response.raise_for_status()
    except httpx.HTTPStatusError:
        if response.status_code == 430:
            print(url, response.text, flush=True)
        return links

    soup = BeautifulSoup(response.text, "html.parser")

    for link in soup.find_all("a"):
        href = link.get("href", "")

        if "twitter" not in links:
            if match := RE_TWITTER.search(href):
                links["twitter"] = match.group()

        if "facebook" not in links:
            if match := RE_FACEBOOK.search(href):
                links["facebook"] = match.group()

        if "email" not in links:
            if match := RE_EMAIL.search(href):
                links["email"] = match.groupdict()["email"]

    return links
