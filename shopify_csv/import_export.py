import csv


def load_stores(path):
    with open(path) as csv_file:
        reader = csv.DictReader(csv_file)
        if not reader.fieldnames or "url" not in reader.fieldnames:
            raise ValueError("The field 'url' is not present in the CSV file.")
        for line in reader:
            yield f"https://{line['url']}"


def export_stores(data):
    fieldnames = [
        "url",
        "email",
        "facebook",
        "twitter",
        "title 1",
        "image 1",
        "title 2",
        "image 2",
        "title 3",
        "image 3",
        "title 4",
        "image 4",
        "title 5",
        "image 5",
    ]

    with open("output.csv", "w") as output_file:
        writer = csv.DictWriter(output_file, fieldnames=fieldnames, restval="")
        writer.writeheader()

        for row in data:
            writer.writerow(row)
