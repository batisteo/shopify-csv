import re
from urllib.parse import urljoin

import httpx
from bs4 import BeautifulSoup

RE_PRODUCT_HANDLE = re.compile(r"\/collections\/all\/products\/(?P<handle>[-a-z0-9]+)")


async def load_top_products(client, url):
    response = await client.get(urljoin(url, "/collections/all"))
    try:
        response.raise_for_status()
    except httpx.HTTPStatusError:
        return {}
    soup = BeautifulSoup(response.text, "html.parser")

    product_handles = []

    for link in soup.find_all("a", class_="product_card")[:5]:
        if match := RE_PRODUCT_HANDLE.search(link.get("href", "")):
            product_handles.append(
                await get_json(client, url, match.groupdict()["handle"])
            )

    products = [
        {f"title {order}": product["title"], f"image {order}": product["image"]}
        for order, product in enumerate(product_handles, start=1)
    ]
    return {k: v for x in products for k, v in x.items()}


async def get_json(client, url, handle):
    try:
        response = await client.get(urljoin(url, f"/products/{handle}.json"))
    except httpx.TimeoutException:
        print(url, " Timeout get_json", flush=True)
        return {}
    try:
        response.raise_for_status()
    except httpx.HTTPStatusError:
        if response.status_code == 430:
            print(url, response.text, flush=True)
        return {}

    product = response.json()["product"]
    return {"title": product["title"], "image": product["images"][0]["src"]}
