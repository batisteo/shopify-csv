import asyncio
import sys
from http import HTTPStatus
from typing import Iterable, Tuple

import httpx

from .import_export import export_stores, load_stores
from .social import find_social_links
from .store import load_top_products

client = httpx.AsyncClient(
    follow_redirects=True, timeout=10.0, transport=httpx.AsyncHTTPTransport(retries=2)
)


class Scraper:
    def __init__(self, stores) -> None:
        self.stores: Iterable[str] = stores
        self.data = {}
        self.http = None

    async def run(self):
        async with client as self.http:
            print("Checking which stores are online…", flush=True)
            online_stores = await self.check_front_pages(self.stores)

            print("Fetching informations about the stores…", flush=True)
            tasks = [self.fetch_info(url) for url in online_stores]
            try:
                await asyncio.gather(*tasks)
            except httpx.PoolTimeout:
                pass

        print("Done.", flush=True)
        self.export()
        print("Exported in output.csv", flush=True)

    async def check_front_pages(self, stores):
        """Filter the stores which landing page responds with 200 OK"""
        responses = await asyncio.gather(
            *[self.check_front_page(url) for url in stores]
        )
        return [url for (url, status) in responses if status == HTTPStatus.OK]

    async def check_front_page(self, url) -> Tuple[str, int]:
        try:
            response = await self.http.get(url)
        except (httpx.ConnectError, httpx.ConnectTimeout, httpx.ReadTimeout):
            return url, 500
        return url, response.status_code

    async def fetch_info(self, url):
        social, top_products = await asyncio.gather(
            find_social_links(self.http, url),
            load_top_products(self.http, url),
        )
        self.data[url] = {**social, **top_products}

    def export(self):
        export_stores([{"url": url, **fields} for url, fields in self.data.items()])


def run():
    try:
        csv_path = sys.argv[1]
    except IndexError:
        csv_path = "examples/stores.csv"

    stores = load_stores(csv_path)
    print("CSV file read.")
    asyncio.run(Scraper(stores).run())
